export function selectBook(book) {
    // selectBook is an ActionCreator, it needs to return an action,
    // an object with type property.

    //actionCreator zwraca akcje - typ akcji + dobindowane dane (type musi byc)

    //akcja automatycznie jest wysylana do wszystkich reducerow
    return {
        type: 'BOOK_SELECTED',
        payload: book
    };
}